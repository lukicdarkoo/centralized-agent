    private Schedule localChoice(List<Schedule> neighbors, Schedule original, double p) {
    	Schedule bestSchedule = null;
    	List<Schedule> bestSchedules = new ArrayList<Schedule>();
    	
    	// Find the best
    	for (Schedule schedule: neighbors) {
    		if (bestSchedule == null || schedule.cost() < bestSchedule.cost()) {
    			bestSchedule = schedule;
    		}
    	}
    	
    	// Find all equal to the best
    	if (bestSchedule != null && random.nextFloat() < p) {
	    	for (Schedule schedule: neighbors) {
	    		 if (schedule.cost() == bestSchedule.cost()) {
	    			 bestSchedules.add(schedule);
	    		}
	    	}
	    	return bestSchedules.get(bestSchedules.size());
    	}
    	
    	// Pick something
    	return original;
    }
    
    
    
	private static TaskFraction findPair(List<TaskFraction> tasks, TaskFraction task) {
		for (TaskFraction pairTask : tasks) {
			if (pairTask.task == task.task && pairTask.type != task.type) {
				return pairTask;
			}
		}
		// This should never happen, so let's scream ASAP!
		new Exception("Pair task of " + task + " is not found!");
		return null;
	}
	
		public List<Schedule> makeChildren(Random random, int numberOfchildren) {
		List<Schedule> children = new ArrayList<Schedule>();

		for (int i = 0; i < numberOfchildren * 0.1; i++) {
			Schedule schedule = this.clone();
			
			Vehicle vehicleFrom = randomVehicle(random, true);
			Vehicle vehicleTo = randomVehicle(random, false);
			TaskFraction task = randomTask(random, vehicleFrom);
			schedule.fullStrategy.get(vehicleFrom).remove(task);
			schedule.fullStrategy.get(vehicleFrom).remove(task.pair);
			schedule.fullStrategy.get(vehicleTo).add(task);
			schedule.fullStrategy.get(vehicleTo).add(task.pair);
			
			if (schedule.checkConstraints())
				children.add(schedule);
			else
				i--;
		}
		
		for (int i = 0; i < numberOfchildren * 0.9; i++) {
			Schedule schedule = this.clone();
			Vehicle vehicle = randomVehicle(random, true);
			Collections.swap(schedule.fullStrategy.get(vehicle), 
					random.nextInt(fullStrategy.get(vehicle).size()), 
					random.nextInt(fullStrategy.get(vehicle).size()));
			
			if (schedule.checkConstraints())
				children.add(schedule);
			else
				i--;
		}
		
		
		return children;
	}