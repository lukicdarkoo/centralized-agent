package template;

//the list of imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import logist.LogistSettings;

import logist.behavior.CentralizedBehavior;
import logist.agent.Agent;
import logist.config.Parsers;
import logist.simulation.Vehicle;
import logist.plan.Plan;
import logist.task.TaskDistribution;
import logist.task.TaskSet;
import logist.topology.Topology;

enum PlanningStrategy {
	GIVEN, GA
}

@SuppressWarnings("unused")
public class CentralizedTemplate implements CentralizedBehavior {	
    private Topology topology;
    private TaskDistribution distribution;
    private Agent agent;
    private long timeout_setup;
    private long timeout_plan;
    private Random random;
    
    @Override
    public void setup(Topology topology, TaskDistribution distribution, Agent agent) {
        
        // this code is used to get the timeouts
        LogistSettings ls = null;
        try {
            ls = Parsers.parseSettings("config/settings_default.xml");
        }
        catch (Exception exc) {
            System.out.println("There was a problem loading the configuration file.");
        }
        
        this.timeout_setup = ls.get(LogistSettings.TimeoutKey.SETUP);
        this.timeout_plan = ls.get(LogistSettings.TimeoutKey.PLAN);
        this.random = new Random(100);
        this.topology = topology;
        this.distribution = distribution;
        this.agent = agent;
    }

    @Override
    public List<Plan> plan(List<Vehicle> vehicles, TaskSet tasks) {
        long time_start = System.currentTimeMillis();
        
        List<Plan> plans = null;
        
        // for (int i = 0; i < 100; i++)
        	plans = gaPlan(vehicles, tasks);
    	
        long time_end = System.currentTimeMillis();
        long duration = time_end - time_start;
        System.out.println("The plan was generated in "+duration+" milliseconds.");
        
        return plans;
    }

    private Schedule localChoice(List<Schedule> neighbors, Schedule original, double p) {
    	Schedule bestSchedule = null;
    	for (Schedule schedule: neighbors) {
    		if (bestSchedule == null || schedule.cost() < bestSchedule.cost()) {
    			bestSchedule = schedule;
    		}
    	}
    	
    	if (bestSchedule != null && random.nextFloat() < p) {
        	return bestSchedule;
    	}
    	else {
    		return original;
    	}
    }
    
    private List<Plan> gaPlan(List<Vehicle> vehicles, TaskSet tasks) {
    	Schedule schedule = new Schedule(vehicles, tasks);
    	List<Plan> plans = new ArrayList<Plan>();
    	
    	System.out.println("Time for planning: " + timeout_plan);
    	
    	double timeStart = System.currentTimeMillis();
    	
    	// Create initial generation
    	List<Schedule> generation = new ArrayList<Schedule>();
    	Schedule bestSchedule = schedule;
    	generation.addAll(schedule.makeChildren(random, 70));
    	
    	double cost = 10000000;
    	double newCost = 0;
    	
    	// Evolve
    	int i = 0;
    	while (System.currentTimeMillis() - timeStart < timeout_plan - 100) {
    		int nBest = 2;
    		int nChildren = 10;
    		
    		List<Schedule> motherIndividuals = getNBest(generation, nBest);
    		generation.clear();
    		
    		for (Schedule mother : motherIndividuals) {
    			generation.addAll(mother.makeChildren(random, nChildren));
    		}
    		
    		newCost = Collections.min(generation).cost();
    		if (newCost < cost) {
    			System.out.println("#" + i + " (" + ((System.currentTimeMillis() - timeStart) / 1000) + ", " + newCost + ")");
    			cost = newCost;
    			bestSchedule = Collections.min(generation);
    		}
    		i++;
    	}

    	// Print statistics
    	System.out.println("Total cost: " + bestSchedule.cost());
    	
    	// Export plan (according to vehicle list)
    	for (Vehicle vehicle : vehicles) {
    		plans.add(bestSchedule.plan(vehicle));
    	}
    	return plans;
    }
    
    private List<Schedule> getNBest(List<Schedule> schedules, int n) {
    	Collections.sort(schedules);
    	return new ArrayList<Schedule>(schedules.subList(0, n));
    }
}
